package airports.impl;

import java.util.List;

public interface IDAO {
	List<String> getAirport(String code);
}
