package airports.impl;

import java.util.List;

public class IFindAirportsImpl implements IFindAirports {
	private IDAO dao;

	public IFindAirportsImpl(IDAO dao) {
		this.dao = dao;
	}

	@Override
	/**
	 * Find Ariports Service Depends on DAO As per unit testing rules you dont test
	 * the DAO as we 1. Mocking is faster testing 2. We are doing unit testing and
	 * not integration testing 3. We are clear that DOA is a dependency 4. We are
	 * having contract with the dao it has to perform as per the dao 5. We are
	 * having the a clarity that our test are our boundaries and not the dao
	 * boundaries
	 */
	public List<String> findAirportByCode(String code) {
		checkBlank(code);
		checkNull(code);
		return dao.getAirport(code);
	}

	private void checkNull(String code) {
		if (code == null)
			throw new IllegalArgumentException();
	}

	private void checkBlank(String code) {
		if (code != null && code.equals(""))
			throw new IllegalArgumentException();
	}
}
