package airports.impl;

import java.util.List;

public interface IFindAirports {
	List<String> findAirportByCode(String code);
}
