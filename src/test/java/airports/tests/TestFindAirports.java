package airports.tests;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import airports.impl.IDAO;
import airports.impl.IFindAirports;
import airports.impl.IFindAirportsImpl;

@DisplayName("Customer should be able find Airports")
class TestFindAirports {
	IFindAirports airportImpl = null;
	IDAO dao = null;

	@BeforeEach
	void init() {
		// i trust that the dao is also tested an asper contract
		// i will trust dao returns right things
		/**********************************************************/
		dao = Mockito.mock(IDAO.class);
		List<String> airports = new ArrayList<String>();
		airports.add("LAX1");
		airports.add("LAX2");
		Mockito.when(dao.getAirport("LAX")).thenReturn(airports);
		/**********************************************************/
		airportImpl = new IFindAirportsImpl(dao);
	}

	@DisplayName("Should Return a List of Airports if correct code or right codes are passed")
	@Test
	/**
	 * if all given situations and right data passed airport should return a list of
	 * airport 1 or more
	 */
	void testFindAirportsByCode() {
		List<String> airports = airportImpl.findAirportByCode("LAX");
		assertFalse(airports.isEmpty());
	}

	@DisplayName("Should Throw IllegalArgumentException if passed code is blank")
	@Test
	/**
	 * if your implementation does not throw the illegal argument exception then
	 * this test case will fail ...
	 */
	void testFindAirportByCodeWithBlankParameter() {
		assertThrows(IllegalArgumentException.class, () -> {
			airportImpl.findAirportByCode("");
		});
	}

	@DisplayName("Should Throw IllegalArgumentException if passed code is null")
	@Test
	/**
	 * This is also a negative test case for checking if user passes the code as
	 * null is a proper exception thrown
	 */
	void testFindAirportByCodeWithNullParameter() {
		assertThrows(IllegalArgumentException.class, () -> {
			airportImpl.findAirportByCode(null);
		});
	}
}
